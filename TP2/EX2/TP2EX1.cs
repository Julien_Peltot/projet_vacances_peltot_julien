﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp11
{
    class TP2EX1
    {
        static void Main(string[] args)
        {

            string ma_phrase;
            char premiere_lettre, derniere_lettre;

            Console.WriteLine("Saisir une phrase");
            ma_phrase = Console.ReadLine();
            Console.WriteLine("Vous avez saisi : {0}", ma_phrase);

            premiere_lettre = ma_phrase[0];
            derniere_lettre = ma_phrase[ma_phrase.Length - 1];

            -------------------

            if (premiere_lettre >= 'A' && premiere_lettre <= 'Z')
            {
                Console.WriteLine("Votre phrase comence par une majuscule");
            }

            else
            {
                Console.WriteLine("Votre phrase ne commence pas par une majuscule");
            }
            ---------------------------

            if (derniere_lettre == '.')
            {
                Console.WriteLine("Votre phrase fini par un point");
            }

            else
            {
                Console.WriteLine("Votre phrase ne fini pas par un point");
            }
            ------------------------------
            Console.ReadKey();
        }
    }
}
