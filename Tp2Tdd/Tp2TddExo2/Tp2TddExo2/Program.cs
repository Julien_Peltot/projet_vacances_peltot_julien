﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2TddExo2
{
    public class Program
    {
        public static int[] Fonction_initialiser(int[] tableau, int longueur_tableau)
        {
           
            int increment, valeur_tableau;
            string valeur_tableau_as_string;

            for (increment = 0; increment <= 9; increment++)
            {
                string phrase_boucle = String.Format("Saisir la valeur {0} dans le tableau : ", increment + 1);
                Console.WriteLine(phrase_boucle);

                valeur_tableau_as_string = Console.ReadLine();
                int.TryParse(valeur_tableau_as_string, out valeur_tableau);
                tableau[increment] = valeur_tableau;

            }
            return tableau;
        }

        public static int Fonction_recherche(int valeur_rechercher, int position_valeur_rechercher, 
                                              int[] tableau, int longueur_tableau)        
        {
            int increment=0;

            while ((increment < longueur_tableau) && (position_valeur_rechercher == -1))
            {
                if (tableau[increment] == valeur_rechercher)
                {
                    position_valeur_rechercher = increment + 1;
                }

                increment++;
            }

            if (position_valeur_rechercher == -1)
            {
                string phrase_non = String.Format("La valeur {0} n'est pas dans le tabeau ", valeur_rechercher);
                Console.WriteLine(phrase_non);
            }
            else
            {
                string phrase_oui = String.Format("La valeur {0} est a la position {1} ", valeur_rechercher, position_valeur_rechercher);
                Console.WriteLine(phrase_oui);
            }

            return position_valeur_rechercher;
           

        }
     
        static void Main(string[] args)
        {
            int valeur_rechercher, position_valeur_rechercher = -1;
            string valeur_rechercher_as_string;
            const int longueur_tableau = 10; int[] tableau = new int[longueur_tableau];

            Fonction_initialiser(tableau, longueur_tableau);

            string phrase_recherche = String.Format("Saisir la valeur a rechercher");
            Console.WriteLine(phrase_recherche);

            valeur_rechercher_as_string = Console.ReadLine();
            int.TryParse(valeur_rechercher_as_string, out valeur_rechercher);
           
            Fonction_recherche(valeur_rechercher,position_valeur_rechercher, tableau, longueur_tableau);

            Console.ReadKey();

        }
    }
}
