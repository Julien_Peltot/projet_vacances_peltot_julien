﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Tp2TddExo2;


namespace Tp2TddExo2Test
{
    [TestClass]
    public class Test
    {

        [TestMethod]
        public void Test_tab()
        {
            int[] TestTableau = new int[] { 10, 10, 11, 12, 13, 14 };

            //si deux valeur identique il prends la 1er
            Assert.AreEqual(1, Program.Fonction_recherche(10, -1, TestTableau, 6));

            // deux valeur identique il ne prends pas les autres
            Assert.AreNotEqual(2, Program.Fonction_recherche(10, -1, TestTableau, 6));

            // n'est pas égal a la positon voulu   
            Assert.AreNotEqual(1, Program.Fonction_recherche(12, -1, TestTableau, 6));

            //si valeur inexisante retourne -1
            Assert.AreEqual(-1, Program.Fonction_recherche(4, -1, TestTableau, 6));

            // marche si valeur et position coherente  
            Assert.AreEqual(6, Program.Fonction_recherche(14, -1, TestTableau, 6));
        }
    }
    
}
