﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tp2TddExo1;

namespace Tp2TddExo1Test

{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void Test_maj()
        {
            string chaine_de_caractere_valide = "Lihubgygv gbubuy";
            string chaine_de_caractere_invalide = "dihubgygv gbubuy";
            Assert.IsTrue(Program.Fonction_maj(chaine_de_caractere_valide));
            Assert.IsFalse(Program.Fonction_maj(chaine_de_caractere_invalide));
        }
   
        [TestMethod]
        public void Test_point()

        {
            string chaine_de_caractere_valide = "vihubgygv gbubuy.";
            string chaine_de_caractere_invalide = "vihubgygv gbubuy";
            Assert.IsTrue(Program.Fonction_point(chaine_de_caractere_valide));
            Assert.IsFalse(Program.Fonction_point(chaine_de_caractere_invalide));
        }




    }
}
