﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2TddExo1

{
    public class Program
    {

        public static bool Fonction_maj(string chaine_de_caractere)

        {
            char premiere_lettre = chaine_de_caractere[0] ;


            if (premiere_lettre >= 'A' && premiere_lettre <= 'Z')
            {
                Console.WriteLine("Votre phrase comence par une majuscule");
                return true;
            }

            else
            {
                Console.WriteLine("Votre phrase ne commence pas par une majuscule");
                return false;
            }
            
        }
        public static bool Fonction_point(string chaine_de_caractere)

        
        {
            char derniere_lettre;
            derniere_lettre = chaine_de_caractere[chaine_de_caractere.Length - 1];

            if (derniere_lettre == '.')
            {
                Console.WriteLine("Votre phrase fini par un point");
                return true;
            }

            else
            {
                Console.WriteLine("Votre phrase ne fini pas par un point");
                return false;
            }


        }

        static void Main(string[] args)
        {
            string chaine_de_caractere;

            Console.WriteLine("Saisir une phrase");
            chaine_de_caractere = Console.ReadLine();
            Console.WriteLine("Vous avez saisi : {0}", chaine_de_caractere);
            Fonction_maj(chaine_de_caractere);
            Fonction_point(chaine_de_caractere);
            Console.ReadKey();
        }
    }
}
